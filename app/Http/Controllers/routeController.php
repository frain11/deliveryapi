<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\routes;
use App\temproutes;
use App\newkeys;
class routeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      echo "Welcome to delivery API. Please create your account or login your existing account!";
       
    }
    public function getRoute(Request $request){
        $ifExist =routes::Where('destination','=',strtoupper($request->origin))
        ->first();
if($ifExist ==null){
       return json_encode("No Available Routes");
        }else{
            $ifExist =routes::Where('destination','=',strtoupper($request->origin))
            ->first();
    if($ifExist ==null){
           return json_encode("No Available Routes");
            }else{
            $destination = strtoupper($request->destination);
            $origin = strtoupper($request->origin);
            $loop = 0;
            $rout = routes::select('origin')->groupBy('origin')->get();
            $loop = count($rout);
            return $this->availablePath($destination, $origin, $loop);
            
            }
        }
    }

    public function availablePath($to, $from, $loop){
        $origins= array($from);
        $dis = $to;
        $new = $from;
        $mainOrigin = $from;
        $loopcount = $loop;
        $resultCounter = 0;
        $lastKeyD = array();
        $fordelete = array();
        $lRoute= "";
        temproutes::truncate();
            for($i = 0; $i < $loopcount; $i++){

                foreach($origins as $origin){
                  
                    $identifiers = routes::where('origin',$origin)->get();;
                    foreach($identifiers as $identify){
                    
                        $identifier = $identify->destination;
                        $distance = $identify->distance;
                        $addKey = new newkeys;
                        $addKey->origin = $identifier;
                        $addKey->save();
                        $ifExist = temproutes::where('lastKey','=', $origin)
                        ->where('nextKey','=', $identifier)->get();
                        $lastroute = temproutes::where('nextKey', $origin)->get();
                    if(count($lastroute) == 0){
                        $temp = new temproutes;
                        $temp->lastKey = $origin;
                        $temp->nextKey = $identifier;
                        $temp->distance = $distance;
                        if($origin == $mainOrigin){
                            if(count($ifExist) == 0){
                            $temp->route = $new . $identifier;
                            $temp->sequence =  $new ."-". $identifier;
                            if($mainOrigin == $identifier){

                            }else{
                                $temp->save();
                            }
                        
                            }
                        } 
                    }else{
                            if($mainOrigin == $identifier){
                            
                        
                        }else{
                            foreach($lastroute as $route){
                                $countRoute = temproutes::select('nextKey')->where('nextKey', $to)->get();
                                $rowCount =count($countRoute);
                                if($rowCount== 10){
                                    newkeys::truncate();
                                    return $this->showResult($from, $to);
                                }
                                $temp2 = new temproutes;
                                $lastKey = $route->lastKey;
                                $lastDis= $route->distance;
                                $lRoute = $route->route;
                                $seq = $route->sequence;
                                $cRoute = $lRoute . $identifier;
                                $temp2->lastKey = $origin;
                                $temp2->nextKey = $identifier;
                                $temp2->distance = $lastDis + $distance; 
                                $routeCheck = temproutes::where('route', $cRoute)->get();
                                if(count($routeCheck) == 0){
                                    if(preg_match("/$identifier/",$lRoute)){

                                    }else{
                                        if($dis == $origin){
                                           
                                          
                                        }else{
                                            array_push($lastKeyD, $lRoute);
                                        
                                        }
                                    
                                    $temp2->route = $lRoute . $identifier;
                                    $temp2->sequence = $seq ."-" . $identifier;
                                    $temp2->save();
                                    
                                   
                                    }
                                }
                            }
                        }
                        }

                        

                    }
                    temproutes::whereIn('route', $fordelete)->delete();
                    $fordelete = $lastKeyD;
                    
            } 
            
            $newKeys = newkeys::select('origin')->groupBy('origin')->get();
            $keys = array();
            foreach($newKeys as $newKey){
                $keys[] = $newKey->origin;
            }
                $origins = $keys;
                newkeys::truncate();
            }
            return $this->showResult($from, $to);
           
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ifExist =routes::where('origin','=',$request->origin)
                          ->where('destination','=',$request->destination)
                          ->first();
        if($ifExist ==null){
            $ifExist =routes::where('origin','=',$request->origin)
                          ->where('destination','=',$request->destination)
                          ->first();
        if($ifExist ==null){
            return  json_encode("Please Enter Valid Origin");
        }else{
            $route = new routes;
            $route->origin = strtoupper($request->origin);
            $route->destination = strtoupper($request->destination);
            $route->distance = $request->distance;
            $route->save();
    
            $route1 = new routes;
            $route1->origin = strtoupper($request->destination);
            $route1->destination = strtoupper($request->origin);
            $route1->distance = $request->distance;
            $route1->save();
            $newRoute['Route Created'] = array(['Origin'=>strtoupper($request->origin),
                               'destination'=>strtoupper($request->destination),
                               'distance'=>$request->distance]);
    
            return  $newRoute;
        }

        }else{
            return  json_encode("Route already exists");
        }                  
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showResult($from, $to)
    {
        $path = array();
        $routes = temproutes::select('sequence','distance', 'lastKey', 'nextKey')->where('nextKey',$to)->orderBy('distance','ASC')->get();
        foreach($routes as $route){
            $origin = $route->lastKey;
            $destination = $route->nextKey;
            if($from == $origin & $destination == $to){

            }else{
                $path[] =$route;
            }
        }
        $avalablePath = $path;

        for($i = 1;$i<=5; $i++){
            $hours =  floor($avalablePath[$i]->distance /60);
            $min = $avalablePath[$i]->distance - ($hours * 60);
            $cost = ($avalablePath[$i]->distance/25) * 1.25;
            $others['Other Routes'][]=array('Route' => $avalablePath[$i]->sequence, 
            'Distance'=> $avalablePath[$i]->distance ." ". "KM",
            'Time'=>$hours.":".$min." " . "HRS",
            'Cost'=>$cost." " . "USD");    
        }



        $hours =  floor($avalablePath[0]->distance /60);
        $min = $avalablePath[0]->distance - ($hours * 60);
        $cost = ($avalablePath[0]->distance/25) * 1.25;
        $shortest['Shortest Route']=array('Route' => $avalablePath[0]->sequence, 
        'Distance'=> $avalablePath[0]->distance ." ". "KM",
        'Time'=>$hours.":".$min." " . "HRS",
        'Cost'=>$cost." " . "USD");
        $Routes['Routes']=array($shortest,$others);
        temproutes::truncate();
        return $Routes;
        }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {    
        $ifExist =routes::where('destination','=',strtoupper($request->destination))
        ->first();
if($ifExist ==null){
    return json_encode("No Available Routes");
}else{
    $route = routes::where('origin','=',$request->origin)
    ->where('destination','=',$request->destination)
    ->first();
    $route->distance = $request->distance;
    $route->save();

    $route1 = routes::where('origin','=',$request->destination)
    ->where('destination','=',$request->origin)
    ->first();
    $route1->distance = $request->distance;
    $route1->save();
    $newRoute['Route Distance Updated'] = array(['Origin'=>$request->origin,
                       'destination'=>$request->destination,
                       'distance'=>$request->distance]);

    return  $newRoute;
}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        routes::where('origin','=',$request->origin)
                          ->where('destination','=',$request->destination)
                          ->delete();

        routes::where('origin','=',$request->destination)
                          ->where('destination','=',$request->origin)
                          ->delete();                
        $deleteRoute['Route Deleted'] = array(['Origin'=>$request->origin,
                    'destination'=>$request->destination]);
    
        return  $deleteRoute;

    }
}
