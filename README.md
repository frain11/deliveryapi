# README #

### Instructions ###

* Import Dump database on your local
* Open .env and add your db credentials
* Run php artisan serve
* Open POSTMAN
* Create account. (To create account go to (http://localhost:8000/api/register) using POST request use this parameters(email,password,c_password).
* To Login go to (http://localhost:8000/api/login) using POST request use this parameters(email,password).
* To get routes of the specific origin and Destination go to (http://127.0.0.1:8000/api/route)  using POST request use this parameters(origin,destination).
* To add routes go to (http://127.0.0.1:8000/api/add) using POST request  use this parameters(origin, destination, distance)(Take note that use Point that are already exists on DB table routes as a origin).
* To update routes distance go to (http://127.0.0.1:8000/api/update)  using POST request  use this parameters(origin, destination, distance).
* To delete routes go to (http://127.0.0.1:8000/api/delete) using POST request use this parameters(origin,destination).
* For authorization use Bearer Token and put the token provided after you register or login.
* Use provided Road Map as a reference on testing.




